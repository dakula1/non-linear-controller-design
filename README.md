# README #
The source code in this package has a Sliding Mode Controller which is designed for a wheeled mobile robot which can track any given trajectory and also avoid obstacles along its path.This Package was designed by Dhanraj Akula as part of the term project for the course Non linear control.


### How do I get set up? ###

The code is set up as a mablab scripts in the folder src/ and the file script_mobilerobot should be run in order to run the simulation.Make sure that all the files in the src are placed in the same folder while running the script_mobilerobot. 


### Complete Report  ###

Along with the source code there is a file named Project report which contains the complete design of the controller