function [xdot]=robot_Controller1(t,x)
global lamda k1 neta e;
t;
%refference signal
Xr=10*cos(t);
Yr=10*sin(t);
Xr_dot=-10*sin(t);
Yr_dot=10*cos(t);
Yr_dot_dot=-10*sin(t);
%placing the obstacle 
X0=-4;
Y0=-9;
%CALCULATING THE DETECTION REGION 
X_org=x(1)+Xr';
Y_org=x(2)+Yr';
dro=sqrt((X_org-X0)^2 + (Y_org-Y0)^2);

L=2;
l=1;
%taking the appropiate action based on the obstacle position 
if(dro>l && dro <L )
dro;
V_obx=(4*(L^2-l^2)*(dro^2-L^2)*(x(1)-X0))/((dro^2-l^2)^3);
V_oby=(4*(L^2-l^2)*(dro^2-L^2)*(x(2)-Y0))/((dro^2-l^2)^3);
alpha=1;
beta=0;
else
    V_obx=0;
    V_oby=0;
    alpha=0;
    beta=1;
end 




% Xr=10*cos(t);
% Yr=10*sin(t);


Vr=sqrt(Xr_dot^2 + Yr_dot^2);
theta_r=0.1;

s1=x(1);
 e=0.1;
 %k1=6;
 k1=1;
u1=-Vr + (sqrt( (x(3)+Yr_dot)^2 +(-k1*tanh(s1/e) +Xr_dot)^2 ));
 lamda=3;
s2=lamda*x(2)+x(3);
 neta=3;
u2=((Yr_dot_dot  -lamda*x(3)-neta*tanh(s2/e) )/(sqrt( (u1+Vr)^2 - (x(3)+Yr_dot)^2 )) );

%equation of the error dynamics of the kinametic model of mobile robot 

xdot(1)=beta*(-k1*tanh(x(1)/e))+alpha*V_obx;
xdot(2)=beta*x(3)+alpha*V_oby;
xdot(3)=beta*((sqrt((u1+Vr)^2-(x(3)+Yr_dot)^2))*u2-Yr_dot_dot)+alpha*atan2(-x(1),-x(2));

xdot=xdot';
