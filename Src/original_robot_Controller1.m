function [xdot]=original_robot_Controller1(t,x,flag,tue,omega,td3)
t;

tuei = interp1(td3,tue,t)
omegai = (interp1(td3,omega,t))
%  tuei =10;
%  omegai =2.9;

%equation of the error dynamics of the kinametic model of mobile robot 

% xdot(1)=sqrt((u1+Vr)^2 - (x(3)+Yr_dot)^2) - Xr_dot;
xdot(1)=tuei*cos(x(3));
xdot(2)=tuei*sin(x(3));
xdot(3)=omegai;

xdot=xdot';
