clear all;
close all;
global lamda k1 neta e;
t=0:0.01:10; %defining the simulation time
Xr=10*cos(t);%refference trajectory X coordinate 
Yr=10*sin(t);%refference trajectory Y coordinate
X=0.5;%setting the initial condition of the 
Y=0.5;
theta=0.2;
Xr_dot=-10*sin(t);
Yr_dot=10*cos(t);
Yr_dot_dot=-10*sin(t);
Vr=sqrt(Xr_dot.^2 + Yr_dot.^2);
theta_r=0.1;
k1=6;
neta=3;
lamda=2;
e=0.1;
% options= odeset('Reltol',0.01,'Stats','on');
 options = odeset('RelTol',1e-8,'AbsTol',1e-8);
[T,X]=ode45('robot_Controller1',t,[X Y theta],options);%Solving the robot model using ode45

plot(T,X)
xlabel('time(sec)')
ylabel('Error ')
grid
vr=10*ones(length(t),1);

%u1=-Vr + (sqrt( (x(3)+Yr_dot)^2 +(-k1*tanh(s1/e) +Xr_dot)^2 ));
u1=-vr + (sqrt( (X(:,3)+Yr_dot').^2 +(-k1*tanh(X(:,1)./e) +Xr_dot').^2 ));
 u2=((Yr_dot_dot')./(sqrt( (u1+vr).^2 - (X(:,3)+Yr_dot').^2 )) ) -lamda*X(:,3)-neta*sign((lamda*X(:,2)+X(:,3))./e);

%% Smooth the trajectory using a filter
Wn = .5; %Hz Desired filter cutoff frequency
dT=0.01;
fs = 1/dT; %Sample rate from the time vector being used

[Bfilt,Afilt] = butter(1, 2*Wn/fs); %Designs a 1st order digital low pass filter with cutoff frequency of 2Hz
u2_f = filtfilt(Bfilt, Afilt, u2);
u1_f = filtfilt(Bfilt, Afilt, u1);
%%
figure(2)
plot(t,[u1_f])
xlabel('Time')
ylabel('Torque')
grid
title('Control Input Torque')

figure(3)
plot(t,[u2_f])
grid
title('Control Input omega')

tau=u1+Vr';
omega=u2+theta_r;
omega_f=u2_f+theta_r;
figure(4)
plot(t,tau)
grid
xlabel('Time')
ylabel('Velocity')
title('Actual Input linear Velocity')

figure(5)
plot(t,[omega_f])
omega_f;
grid
xlabel('Time')
ylabel('Omega')
title('Actual Input Omega')
X_org=X(:,1)+Xr';
Y_org=X(:,2)+Yr';
theta_org=X(:,3)+theta_r;
figure(6)
plot(X_org,Y_org)
hold on 
plot(Xr,Yr)
grid
% hold on 
X0=-4;
Y0=-9;
plot([X0],[Y0],'o')
 xlabel('X-axies')
 ylabel('Y-axies')
 legend('actual trajactory of the robot','the reffrence trajactory of the robot','Obstacle')
% legend('actual trajactory of the robot','the reffrence trajactory of the robot')
title('Trajectory Tracking')
e1=X_org-Xr';
e2=Y_org-Yr';
e3=theta_org-theta_r';
figure(7)
plot(t,e1)
xlabel('Time(s)')
ylabel('position error e1')
grid
title('X axies tracking error')

figure(8)
plot(t,e2)
xlabel('Time(s)')
ylabel('position error e2')
grid
title('Y axis Tracking error')

figure(9)
plot(t,e3)
xlabel('Time(s)')
ylabel('position error e3')
grid
title('Orientation Tracking error')






